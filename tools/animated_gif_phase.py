import imageio
from pygifsicle import optimize

from koreviz import kmode as k
# u = k.kmode('u', 0, nr=500, nphi=500)

solnum = 1
r = 1
idx = argmin(abs(u.r-r))

n = 36
phases = linspace(0,360,n+1)[:-1]

c = 0.9
#limits = [u.utheta[idx,:,:].min()*c, u.utheta[idx,:,:].max()*c]
limits = [0,0]

filenames = []
for j,phase in enumerate(phases):
    
    print(j+1,'/',n)
    u = k.kmode('u', solnum, ntheta=360, nphi=1440, phase = phase) 
    #u.surf(comp='t',r=r,colbar=False,limits=limits)
    u.equat(comp='t',colbar=False,limits=limits)
    filename = f'{j}.png'
    savefig(filename,dpi=120)
    filenames.append(filename)
    close('all')
    
# build gif
with imageio.get_writer('imode.gif', mode='I') as writer:
    for filename in filenames:
        image = imageio.imread(filename)
        writer.append_data(image)


optimize('imode.gif')
